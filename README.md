# Wardrobify

Team:

* Person 1 - Jahaziel Martinez -- Hats
* Person 2 - Miguel Ortiz -- Shoes

## Design

## Shoes microservice

I created a Value Object of the Bin Object so that I could use the Bin information from the wardrobe api microservice and display it in my necessary views. I brought that information to my microservice using a poller, passed it as JSON to the front end using my views and then displayed it using my react code.

## Hats microservice

I created a Value Object of the Location Object so that I could use the Location information from the wardrobe api microservice and display it in my necessary views. I brought that information to my microservice using a poller, passed it as JSON to the front end using my views and then displayed it using my react code.
