import React, {useEffect, useState } from 'react';

function CreateHatForm() {

  const [location, setLocation] = useState('');
  const [fabric, setFabric] = useState('');
  const [styleName, setStyleName] = useState('');
  const [locations, setLocations] = useState([]);
  const [color, setColor] = useState('');
  const [imageUrl, setImageUrl] = useState('');


  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.location = location;
    data.style_name = styleName;
    data.color = color;
    data.image_url = imageUrl;
    data.fabric=fabric;

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const hatResponse = await fetch(hatUrl, fetchOptions);
    if (hatResponse.ok) {
      setStyleName('');
      setImageUrl('');
      setFabric('');
      setColor('');
      setLocation('');
    }
  }

  const handleChangeFabric = (event) => {
    const value = event.target.value;
    setFabric(value);
  }

  const handleChangeStyleName = (event) => {
    const value = event.target.value;
    setStyleName(value);
  }

  const handleChangeColor = (event) => {
    const value = event.target.value;
    setColor(value);
  }
  const handleChangeImageUrl = (event) => {
    const value = event.target.value;
    setImageUrl(value);
  }
  const handleChangeLocation = (event) => {
    const value = event.target.value;
    setLocation(value);
  }
  let dropdownClasses = 'form-select';
  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';

  return (
              <div>
              <form className={formClasses} onSubmit={handleSubmit} id="create-hat-form">
                <h1 className="card-title">Create a New Hat!</h1>

                <p className="mb-3">
                  Now, tell us about your hat.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeStyleName} required placeholder="Hat Style Name" type="text" id="styleName" name="styleName" className="form-control" value={styleName} />
                      <label htmlFor="styleName">Style Name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeColor} required placeholder="Hat Color" type="text" id="color" name="color" className="form-control" value={color} />
                      <label htmlFor="color">Color</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeFabric} required placeholder="Hat Fabric" type="text" id="fabric" name="fabric" className="form-control" value={fabric} />
                      <label htmlFor="fabric">Fabric</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeImageUrl} required placeholder="Image URL" type="text" id="imageUrl" name="imageUrl" className="form-control" value={imageUrl} />
                      <label htmlFor="imageUrl">Image URL</label>
                    <div className="col">
                      <div className="mb-3">
                  <select onChange={handleChangeLocation} value={location } name="location" id="location" className={dropdownClasses} required>
                    <option value="">Choose a Location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.href} value={location.href}>{location.closet_name}</option>
                      )

                    })}
                  </select>
                </div>
                </div>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create a Hat!</button>
              </form>
              <div className={messageClasses} id="success-message">
                Congratulations! You created a hat!
              </div>
              </div>
  );
}

export default CreateHatForm;
