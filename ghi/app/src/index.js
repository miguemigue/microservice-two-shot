import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
async function loadHatsandShoes() {
  const response1 = await fetch('http://localhost:8090/api/hats/');
  const response2 = await fetch('http://localhost:8080/api/shoes/');
  if (response1.ok && response2.ok ) {
    const data = await response1.json();
    const data2 = await response2.json();
    root.render(
      <React.StrictMode>
        <App hats={data.hats} shoes={data2.shoes} />
      </React.StrictMode>
    );
} else {
  console.error(response1, response2);
}}
loadHatsandShoes();





